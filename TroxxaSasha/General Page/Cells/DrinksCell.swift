import UIKit


class DrinksCell: UITableViewCell {
    
    var imageDrink = UIImageView()
    var drinkName = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        addSubview(imageDrink)
        imageDrink.backgroundColor = .lightGray
        
        addSubview(drinkName)
        drinkName.textColor = UIColor(red: 0.496, green: 0.496, blue: 0.496, alpha: 1)
        drinkName.font = UIFont.systemFont(ofSize: 16, weight: .regular)
        drinkName.textAlignment = .left
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        imageDrink.pin.vertically(5).left(20).width(100)
        drinkName.pin.left(to: imageDrink.edge.right).marginLeft(20).right(20).vertically(45)
    }
}
