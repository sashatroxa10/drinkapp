import UIKit
import Foundation


struct AllData : Codable {
    var drinks: [CockTailDetail]?
}

struct CockTailDetail : Codable {
    let strDrink: String?
    let strDrinkThumb: String?
    let idDrink: String?
}

struct AllCategories : Codable {
    var drinks: [Categories]?
}

struct Categories : Codable {
    let strCategory: String?
}
