import UIKit

class FilterCell: UITableViewCell {
    
    let groupCellname = UILabel()
    let checkButton = UIButton()
    var tapAction: ((UITableViewCell) -> Void)?

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
      
        addSubview(groupCellname)
        groupCellname.font = UIFont.systemFont(ofSize: 16, weight: .regular)
        groupCellname.textColor = UIColor(red: 0.496, green: 0.496, blue: 0.496, alpha: 1)
        groupCellname.text = "Buhlo"
        
        addSubview(checkButton)
        checkButton.setImage(UIImage(named: "check"), for: .normal)
        
        checkButton.addTarget(self, action: #selector(buttonTap), for: .touchUpInside)
        }
    
    @objc func buttonTap(sender: AnyObject) {
      tapAction?(self)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        groupCellname.pin.left(35).vCenter().right(75).height(20)
        checkButton.pin.right(30).width(25).vCenter().height(20)
    }
}
